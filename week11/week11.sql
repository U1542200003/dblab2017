use movie_db;
# 6. Show the thriller films whose budget is greater than 25 million$.	 
select title
from movies join genres on movies.movie_id=genres.movie_id
where budget>25000000 and genre_name="Thriller";

# 7. Show the drama films whose language is Italian and produced between 1990-2000.	
select title
from movies join genres on movies.movie_id=genres.movie_id
where year between 1990 and 2000 and genre_name="Drama" and movie_id in (
select movie_id
from movies join languages on movies.movie_id=languages.movie_id
where language_name="Italian");

select title 
from movies
where year between 1990 and 2000 and movie_id in (
select genres.movie_id
from genres join languages on genres.movie_id=languages.movie_id
where genre_name="Drama" and language_name="Italian");

# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	
select title
from movies
where oscars>3 and movie_id in(
select movie_id
from movie_stars join stars on movie_stars.star_id=stars.star_id
where star_name="Tom Hanks");

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
select title
from movies join genres on movies.movie_id=genres.movie_id
where duration between 100 and 200  and genre_name="History" and movies.movie_id in(
select movie_id 
from countries join producer_countries on countries.country_id=producer_countries.country_id
where country_name="USA");

# 10.Compute the average budget of the films directed by Peter Jackson.
select title,budget
from movies
where movie_id in(
select movie_id
from directors join movie_directors on directors.director_id=movie_directors.director_id
where director_name="Peter Jackson");

select avg(budget)
from movies
where movie_id in(
select movie_id
from directors join movie_directors on directors.director_id=movie_directors.director_id
where director_name="Peter Jackson");

# 11.Show the Francis Ford Coppola film that has the minimum budget.
select title,budget
from movies
where movie_id in(
select movie_id
from directors join movie_directors on directors.director_id=movie_directors.director_id
where director_name="Francis Ford Coppola")
order by budget
limit 1;

# 12.Show the film that has the most vote and has been produced in USA.
select title,votes
from movies
where movies.movie_id in(
select movie_id 
from countries join producer_countries on countries.country_id=producer_countries.country_id
where country_name="USA")
order by votes desc
limit 1;